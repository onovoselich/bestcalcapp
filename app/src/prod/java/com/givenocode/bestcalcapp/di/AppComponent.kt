package com.givenocode.bestcalcapp.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        DataModule::class))
interface AppComponent : BaseAppComponent
