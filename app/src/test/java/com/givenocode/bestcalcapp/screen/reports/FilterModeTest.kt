package com.givenocode.bestcalcapp.screen.reports

import com.givenocode.bestcalcapp.R
import org.junit.Assert.*
import org.junit.Test

class FilterModeTest {

    val modeToResId = mapOf(
            FilterMode.DAY to R.id.group_day,
            FilterMode.WEEK to R.id.group_week,
            FilterMode.MONTH to R.id.group_month,
            FilterMode.YEAR to R.id.group_year,
            FilterMode.ALL to R.id.group_all
    )

    @Test
    fun testFromId() {
        modeToResId.forEach { mode, id ->
            assertEquals(mode, fromId(id))
        }
    }

    @Test(expected = IllegalStateException::class)
    fun testFromIncorrectId() {
        fromId(android.R.id.button1)
    }

    @Test
    fun testToId() {
        modeToResId.forEach { mode, id ->
            assertEquals(id, mode.toId())
        }
    }
}