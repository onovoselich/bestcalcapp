package com.givenocode.bestcalcapp.interactor

import com.givenocode.bestcalcapp.commons.extensions.mock
import com.givenocode.bestcalcapp.data.db.ProductsDao
import com.givenocode.bestcalcapp.data.models.Product
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.*
import org.mockito.Mockito

class ProductsInteractorTest {

    lateinit var productsInteractor: ProductsInteractor
    lateinit var productsDao: ProductsDao

    @Before
    fun setUp() {
        productsDao = mock()
        productsInteractor = ProductsInteractor(productsDao)
    }

    @Test
    fun testFetchCallsAll() {
        val result: Flowable<List<Product>> = Flowable.empty()
        given(productsDao.all()).willReturn(result)

        productsInteractor.fetch().test().await()
                .assertValueCount(0)

        verify(productsDao).all()
        verifyNoMoreInteractions(productsDao)
    }

    @Test
    fun testGetById() {
        val id: Long = 123

        productsInteractor.get(id)

        verify(productsDao).get(id)
        Mockito.verifyNoMoreInteractions(productsDao)
    }

    @Test
    fun testInsert() {
        val product: Product = mock()

        productsInteractor.insert(product)

        verify(productsDao).insert(product)
        verifyNoMoreInteractions(productsDao)
    }

    @Test
    fun testUpdate() {
        val product: Product = mock()

        productsInteractor.update(product)

        verify(productsDao).insert(product)
        verifyNoMoreInteractions(productsDao)
    }

    @Test
    fun testDelete() {
        val id: Long = 123
        val product = Product(id = id)

        productsInteractor.delete(product)

        verify(productsDao).insert(product.copy(deleted = true))
        verifyNoMoreInteractions(productsDao)
    }
}
