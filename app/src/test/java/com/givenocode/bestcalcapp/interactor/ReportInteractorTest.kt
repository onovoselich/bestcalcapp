package com.givenocode.bestcalcapp.interactor

import com.givenocode.bestcalcapp.commons.extensions.mock
import com.givenocode.bestcalcapp.data.db.ProductsDao
import com.givenocode.bestcalcapp.data.db.ReportsDao
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.data.models.ReportItem
import com.givenocode.bestcalcapp.screen.reports.FilterMode
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.ReplaySubject
import org.joda.time.DateTimeUtils
import org.joda.time.LocalDate
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.BDDMockito.given
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions

class ReportInteractorTest {

    private lateinit var reportsInteractor: ReportInteractor
    private lateinit var productsDao: ProductsDao
    private lateinit var reportsDao: ReportsDao

    @Before
    fun setUp() {
        productsDao = mock()
        reportsDao = mock()
        reportsInteractor = ReportInteractor(productsDao, reportsDao)

        given(productsDao.get(PRODUCT_ID)).willReturn(PRODUCT_ITEM)
    }

    @Test
    fun testInsert() {
        reportsInteractor.insert(REPORT_ITEM)

        verify(reportsDao).insert(REPORT_ITEM)
        verifyNoMoreInteractions(reportsDao)
    }

    @Test
    fun testDelete() {
        reportsInteractor.delete(REPORT_ITEM)

        verify(reportsDao).delete(REPORT_ITEM)
        verifyNoMoreInteractions(reportsDao)
    }

    @Test
    fun fetchMultipleEmissions() {
        val subject = ReplaySubject.create<List<ReportItem>>()
        subject.onNext(listOf(REPORT_ITEM))
        subject.onNext(listOf(REPORT_ITEM))
        subject.onNext(listOf(REPORT_ITEM))
        subject.onComplete()
        given(reportsDao.all()).willReturn(subject.toFlowable(BackpressureStrategy.BUFFER))

        reportsInteractor.fetch().test()
                .await()
                .assertValueCount(3)

    }

    @Test
    fun testFetchWhenOneItem() {
        val items = listOf(REPORT_ITEM)
        given(reportsDao.all()).willReturn(Flowable.just(items))

        val expectedQuantity = REPORT_ITEM.quantity
        val expectedPrice = REPORT_ITEM.price * REPORT_ITEM.quantity
        reportsInteractor.fetch().test()
                .await()
                .assertValueCount(1)
                .assertValue { it.size == 1 }
                .assertValue { it[0].name == PRODUCT_ITEM.name }
                .assertValue { it[0].quantity == expectedQuantity }
                .assertValue { it[0].totalPrice == expectedPrice }
    }

    @Test
    fun fetchMultipleItemsSameProduct() {
        val items = listOf(REPORT_ITEM, REPORT_ITEM2)
        given(reportsDao.all()).willReturn(Flowable.just(items))

        val expectedQuantity = REPORT_ITEM.quantity + REPORT_ITEM2.quantity
        val expectedPrice = REPORT_ITEM.price * REPORT_ITEM.quantity + REPORT_ITEM2.price * REPORT_ITEM2.quantity
        reportsInteractor.fetch().test()
                .await()
                .assertValue { it.size == 1 }
                .assertValue { it[0].name == PRODUCT_ITEM.name }
                .assertValue { it[0].quantity == expectedQuantity }
                .assertValue { it[0].totalPrice == expectedPrice }
    }

    @Test
    fun fetchMultipleItemsDifferentProducts() {
        given(productsDao.get(anyLong())).willReturn(PRODUCT_ITEM)
        val items = listOf(REPORT_ITEM, REPORT_ITEM2,
                REPORT_ITEM.copy(productId = 2),
                REPORT_ITEM.copy(productId = 3),
                REPORT_ITEM.copy(productId = 4)
        )
        given(reportsDao.all()).willReturn(Flowable.just(items))

        reportsInteractor.fetch().test()
                .await()
                .assertValue { it.size == 4 }
    }

    @Test
    fun getTabsCountEmpty() {

        given(reportsDao.oldestOrder()).willReturn(null)
        DateTimeUtils.setCurrentMillisFixed(SOME_DATE.toDate().time)

        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.ALL))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.DAY))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.WEEK))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.MONTH))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.YEAR))
    }

    @Test
    fun getTabsCountYesterday() {
        given(reportsDao.oldestOrder()).willReturn(SOME_DATE.withDayOfMonth(10).toDate())
        DateTimeUtils.setCurrentMillisFixed(SOME_DATE.toDate().time)

        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.ALL))
        assertEquals(2, reportsInteractor.getTabsCount(FilterMode.DAY))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.WEEK))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.MONTH))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.YEAR))
    }

    @Test
    fun getTabsCountTwoWeeks() {
        given(reportsDao.oldestOrder()).willReturn(SOME_DATE.withDayOfMonth(4).toDate())
        DateTimeUtils.setCurrentMillisFixed(SOME_DATE.toDate().time)

        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.ALL))
        assertEquals(8, reportsInteractor.getTabsCount(FilterMode.DAY))
        assertEquals(2, reportsInteractor.getTabsCount(FilterMode.WEEK))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.MONTH))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.YEAR))
    }

    @Test
    fun getTabsCountTwoMonths() {
        given(reportsDao.oldestOrder()).willReturn(SOME_DATE.withMonthOfYear(9).toDate())
        DateTimeUtils.setCurrentMillisFixed(SOME_DATE.toDate().time)

        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.ALL))
        assertEquals(31, reportsInteractor.getTabsCount(FilterMode.DAY))
        assertEquals(5, reportsInteractor.getTabsCount(FilterMode.WEEK))
        assertEquals(2, reportsInteractor.getTabsCount(FilterMode.MONTH))
        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.YEAR))
    }

    @Test
    fun getTabsCountTwoYears() {
        given(reportsDao.oldestOrder()).willReturn(SOME_DATE.withYear(2016).toDate())
        DateTimeUtils.setCurrentMillisFixed(SOME_DATE.toDate().time)

        assertEquals(1, reportsInteractor.getTabsCount(FilterMode.ALL))
        assertEquals(13, reportsInteractor.getTabsCount(FilterMode.MONTH))
        assertEquals(2, reportsInteractor.getTabsCount(FilterMode.YEAR))
    }

    @Test
    fun getPageDateRange() {
        DateTimeUtils.setCurrentMillisFixed(SOME_DATE.toDate().time)

        var result = reportsInteractor.getPageDateRange(0, 1, FilterMode.ALL)
        assertNull(result)

        result = reportsInteractor.getPageDateRange(0, 1, FilterMode.DAY)
        assertEquals(SOME_DATE, result!!.first)
        assertEquals(SOME_DATE, result.second)

        result = reportsInteractor.getPageDateRange(1, 4, FilterMode.DAY)
        assertEquals(SOME_DATE.minusDays(2), result!!.first)
        assertEquals(SOME_DATE.minusDays(2), result.second)

        result = reportsInteractor.getPageDateRange(0, 1, FilterMode.WEEK)
        assertEquals(SOME_DATE.withDayOfMonth(9), result!!.first)
        assertEquals(SOME_DATE.withDayOfMonth(15), result.second)

        result = reportsInteractor.getPageDateRange(1, 3, FilterMode.WEEK)
        assertEquals(SOME_DATE.withDayOfMonth(2), result!!.first)
        assertEquals(SOME_DATE.withDayOfMonth(8), result.second)

        result = reportsInteractor.getPageDateRange(0, 1, FilterMode.MONTH)
        assertEquals(SOME_DATE.withDayOfMonth(1), result!!.first)
        assertEquals(SOME_DATE.withDayOfMonth(31), result.second)

        result = reportsInteractor.getPageDateRange(0, 1, FilterMode.YEAR)
        assertEquals(SOME_DATE.withMonthOfYear(1).withDayOfMonth(1), result!!.first)
        assertEquals(SOME_DATE.withMonthOfYear(12).withDayOfMonth(31), result.second)

    }

    companion object {
        val PRODUCT_ID: Long = 1

        val PRODUCT_ITEM = Product("alala", id = PRODUCT_ID)
        val REPORT_ITEM = ReportItem(PRODUCT_ID, 5000, 40)
        val REPORT_ITEM2 = ReportItem(PRODUCT_ID, 4000, 80)

        val SOME_DATE = LocalDate()
                .withDayOfMonth(11)
                .withMonthOfYear(10)
                .withYear(2017)
    }
}
