package com.givenocode.bestcalcapp.commons

import org.junit.Assert.assertEquals
import org.junit.Test
import java.util.*

class ExtensionsTest {

    @Test
    fun toDecimal() {
        assertEquals("0.00", 0.toDecimal())
        assertEquals("0.01", 1.toDecimal())
        assertEquals("10.10", 1010.toDecimal())
        assertEquals("10.01", 1001.toDecimal())
        assertEquals("1.00", 100.toDecimal())
        assertEquals("0.00", (-1).toDecimal())
        assertEquals("0.00", Integer.MIN_VALUE.toDecimal())
    }

    @Test
    fun asPrice_us() {
        assertEquals("$0.00", 0.asPrice(Locale.US))
        assertEquals("$0.01", 1.asPrice(Locale.US))
        assertEquals("$10.10", 1010.asPrice(Locale.US))
        assertEquals("$10.01", 1001.asPrice(Locale.US))
        assertEquals("$1.00", 100.asPrice(Locale.US))
        assertEquals("$0.00", (-1).asPrice(Locale.US))
        assertEquals("$21,474,836.47", Integer.MAX_VALUE.asPrice(Locale.US))
        assertEquals("$0.00", Integer.MIN_VALUE.asPrice(Locale.US))
    }

    @Test
    fun toPrice() {
        assertEquals(0, "".toPrice())
        assertEquals(0, "dfgsdfgsdfg".toPrice())
        assertEquals(0, "0".toPrice())
        assertEquals(100, "1".toPrice())
        assertEquals(55555500, "555555".toPrice())

        assertEquals(0, "0.0".toPrice())
        assertEquals(1230, "12.3".toPrice())
        assertEquals(123, "1.23".toPrice())
        assertEquals(0, "0.00".toPrice())
        assertEquals(100, "1.00".toPrice())
        assertEquals(5555, "55.5555".toPrice())
        assertEquals(9999, "99.99999".toPrice())
        assertEquals(55, ".555555".toPrice())
    }
}
