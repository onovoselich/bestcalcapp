package com.givenocode.bestcalcapp.data.models

import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class ProductTest {

    companion object {
        val NAME = "product_name"
        val PRICE = 1001
        val IMAGE_URL = "http://via.placeholder.com/200x200"

        val PRICE2 = 11
    }

    var product = Product(NAME, PRICE, IMAGE_URL)

    @Before
    fun setUp() {
        product = Product(NAME, PRICE, IMAGE_URL)
    }

    @Test
    fun copy() {
        val copiedProduct = product.copy()

        assertEquals(product.name, copiedProduct.name)
        assertEquals(product.price, copiedProduct.price)
        assertEquals(product.imageUrl, copiedProduct.imageUrl)
    }

    @Test
    fun copyChangeProperty() {
        val copiedProduct = product.copy(price = PRICE2)

        assertEquals(product.name, copiedProduct.name)
        assertEquals(PRICE2, copiedProduct.price)
        assertEquals(product.imageUrl, copiedProduct.imageUrl)
    }

    @Test
    fun hashCodeTest() {
        val anotherProduct = Product(NAME, PRICE, IMAGE_URL)

        val hash1 = product.hashCode()
        val hash2 = anotherProduct.hashCode()

        assertEquals(hash1, hash2)
    }

    @Test
    fun equalsTest() {
        val anotherProduct = Product(NAME, PRICE, IMAGE_URL)

        assertFalse(product === anotherProduct)
        assertTrue(product == anotherProduct)
    }

}