package com.givenocode.bestcalcapp.data.db.converters

import org.junit.Test

import org.junit.Assert.*
import java.util.*

class DateConverterTest {

    companion object {
        val TIMESTAMP:Long = 123456789
        val DATE = Date(TIMESTAMP)
    }

    val dateConverter = DateConverter()

    @Test
    fun fromTimestamp() {
        val convertedDate = dateConverter.fromTimestamp(TIMESTAMP)

        assertEquals(DATE, convertedDate)
    }

    @Test
    fun dateToTimestamp() {
        val convertedTimeStamp = dateConverter.dateToTimestamp(DATE)

        assertEquals(TIMESTAMP, convertedTimeStamp)
    }

}