package com.givenocode.bestcalcapp.mvvm

import com.givenocode.bestcalcapp.interactor.ReportInteractor
import com.givenocode.bestcalcapp.screen.reports.FilterMode
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReportsTabsViewModel @Inject constructor(val reportInteractor: ReportInteractor) {
    var selectedFilter = FilterMode.ALL
        set(value) {
            field = value
            listOfDates = reportInteractor.getTabs(value)
        }

    private var listOfDates = reportInteractor.getTabs(selectedFilter)


    fun getTabsCount() = listOfDates.size

    fun getTabTitle(position: Int): String {
        val dates = listOfDates[position]
        dates.apply {
            return if (dates.first == dates.second) {
                dates.first.toString()
            } else {
                "${dates.first} \n" +
                        "- \n" +
                        "${dates.second}"
            }
        }
        return ""
    }

    fun getPageDateRange(position: Int) = listOfDates[position]

}
