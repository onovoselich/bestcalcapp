package com.givenocode.bestcalcapp.mvvm

import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.commons.loadImg
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.data.models.ReportDisplayItem
import com.givenocode.bestcalcapp.screen.adapter.ProductsAdapter
import com.givenocode.bestcalcapp.screen.adapter.ReportsAdapter

@BindingAdapter("bind:imgUrl")
fun ImageView.bindImgUrl(url: String) {
    loadImg(url)
}

@BindingAdapter("bind:products", "bind:onProductClick", requireAll = true)
fun RecyclerView.bindProducts(items: ObservableArrayList<Product>,
                              listener: ProductsAdapter.OnProductClickListener) {
    adapter = ProductsAdapter(items)
    (adapter as ProductsAdapter).listener = listener
}

@BindingAdapter("bind:reports")
fun RecyclerView.bindReports(items: ObservableArrayList<ReportDisplayItem>) {
    adapter = ReportsAdapter(items)
}

@BindingAdapter("bind:visible")
fun View.bindVisible(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}
