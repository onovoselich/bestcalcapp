package com.givenocode.bestcalcapp.mvvm

import com.givenocode.bestcalcapp.data.models.ReportDisplayItem
import com.givenocode.bestcalcapp.interactor.ReportInteractor
import org.joda.time.LocalDate
import javax.inject.Inject

class ReportsViewModel @Inject constructor(private val reportsInteractor: ReportInteractor)
    : Fetchable<ReportDisplayItem>() {

    override fun fetchItems() = reportsInteractor.fetch()

    fun fetch(startDate: LocalDate, endDate: LocalDate) =
            super.fetch { reportsInteractor.fetch(startDate, endDate) }
}
