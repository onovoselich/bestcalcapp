package com.givenocode.bestcalcapp.mvvm

import android.databinding.ObservableArrayList
import android.databinding.ObservableField
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class Fetchable<T> {

    val loading = ObservableField(false)
    val error = ObservableField<String>()
    val items = ObservableArrayList<T>()

    private val compositeDisposable = CompositeDisposable()

    private fun addDisposable(toDispose: () -> Disposable) {
        compositeDisposable.add(toDispose.invoke())
    }

    fun onDetach() {
        compositeDisposable.clear()
    }

    fun fetch(fetchItemsFun: () -> Flowable<List<T>> = { fetchItems()}) {
        if (loading.get()!!) {
            return
        }
        error.set(null)
        loading.set(true)
        addDisposable {
            fetchItemsFun()
                    .doOnTerminate({ loading.set(false) })      // just in case
                    .subscribe(
                            {
                                // hide loading on firstly emitted (for caching)
                                loading.set(false)
                                items.clear()
                                items.addAll(it)
                            },
                            {
                                loading.set(false)
                                if (it.message != null) {
                                    error.set(it.message)
                                } else {
                                    error.set("")
                                }
                            })
        }
    }

    protected abstract fun fetchItems(): Flowable<List<T>>
}
