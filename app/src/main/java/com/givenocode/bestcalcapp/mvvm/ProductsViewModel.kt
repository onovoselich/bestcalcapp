package com.givenocode.bestcalcapp.mvvm

import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.interactor.ProductsInteractor
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductsViewModel @Inject constructor(private val productsInteractor: ProductsInteractor)
    : Fetchable<Product>() {

    override fun fetchItems() = productsInteractor.fetch()
}
