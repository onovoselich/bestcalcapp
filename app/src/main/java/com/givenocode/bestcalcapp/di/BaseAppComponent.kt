package com.givenocode.bestcalcapp.di

import com.givenocode.bestcalcapp.screen.reports.ReportsFragment
import com.givenocode.bestcalcapp.screen.reports.ReportsTabsFragment
import com.givenocode.bestcalcapp.screen.sale.SaleFragment
import com.givenocode.bestcalcapp.screen.stock.ProductDialogFragment
import com.givenocode.bestcalcapp.screen.stock.StockFragment

interface BaseAppComponent {

    fun inject(saleFragment: StockFragment)
    fun inject(saleFragment: SaleFragment)
    fun inject(productDialogFragment: ProductDialogFragment)
    fun inject(reportsFragment: ReportsFragment)
    fun inject(reportsTabsFragment: ReportsTabsFragment)
}
