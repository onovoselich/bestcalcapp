package com.givenocode.bestcalcapp.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * A module for Android-specific dependencies which require a [android.content.Context] or to create.
 */
@Module
class AppModule(private val application: Application) {

    /**
     * Allow the application context to be injected.
     */
    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

}