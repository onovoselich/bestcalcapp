package com.givenocode.bestcalcapp.commons

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.givenocode.bestcalcapp.R
import com.squareup.picasso.Picasso
import java.io.File


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

fun ImageView.loadImg(imageUrl: String) {
    if (TextUtils.isEmpty(imageUrl)) {
        Picasso.with(context).load(R.drawable.product_placeholder).into(this)
    } else {
        val imageFile = File(imageUrl)
        if (imageFile.exists()) {
            loadImg(imageFile)
        } else {
            Picasso.with(context).load(imageUrl)
                    .placeholder(R.drawable.product_placeholder)
                    .error(R.drawable.product_placeholder)
                    .into(this)
        }
    }
}

fun ImageView.loadImg(imageFile: File) {
    Picasso.with(context).load(imageFile)
            .placeholder(R.drawable.product_placeholder)
            .error(R.drawable.product_placeholder)
            .into(this)
}
