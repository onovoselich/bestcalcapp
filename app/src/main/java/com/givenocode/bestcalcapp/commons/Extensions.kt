package com.givenocode.bestcalcapp.commons

import java.text.NumberFormat
import java.util.*

fun Int.toDecimal() =
        if (this < 0) {
            "0.00"
        } else {
            String.format("%.2f", this / 100f)
        }

@JvmOverloads
fun Int.asPrice(locale: Locale = Locale.getDefault()): String {
    return NumberFormat.getCurrencyInstance(locale)
            .format(if (this > 0) {
                this / 100.0
            } else {
                0
            })
}

fun String.toPrice(): Int {
    var result = 0
    if (isNotEmpty()) {
        result = try {
            (toDouble() * 100).toInt()
        } catch (e: NumberFormatException) {
            0
        }
    }
    return result
}
