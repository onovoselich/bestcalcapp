package com.givenocode.bestcalcapp.screen.adapter

import android.databinding.DataBindingUtil
import android.databinding.ObservableArrayList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.data.models.ReportDisplayItem
import com.givenocode.bestcalcapp.databinding.ItemReportBinding

class ReportsAdapter(val items: ObservableArrayList<ReportDisplayItem>)
    : RecyclerView.Adapter<ReportsAdapter.ReportViewHolder>() {

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemReportBinding = DataBindingUtil.inflate(inflater, R.layout.item_report, parent, false)
        return ReportViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ReportViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ReportViewHolder(var binding: ItemReportBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ReportDisplayItem) = with(itemView) {
            binding.report = item
            binding.executePendingBindings()
        }
    }
}
