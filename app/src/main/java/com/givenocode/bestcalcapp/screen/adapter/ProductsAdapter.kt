package com.givenocode.bestcalcapp.screen.adapter

import android.databinding.DataBindingUtil
import android.databinding.ObservableArrayList
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.databinding.ItemProductBinding
import kotlinx.android.synthetic.main.item_product.view.*

class ProductsAdapter(val items: ObservableArrayList<Product>)
    : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ItemProductBinding = DataBindingUtil.inflate(inflater, R.layout.item_product, parent, false)
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductsAdapter.ProductViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ProductViewHolder(var binding: ItemProductBinding)
        : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Product) = with(itemView) {
            binding.product = item
            binding.executePendingBindings()
            productCard.setOnClickListener { listener?.onProductClick(item) }
        }
    }

    var listener: OnProductClickListener? = null

    interface OnProductClickListener {
        fun onProductClick(product: Product)
    }
}
