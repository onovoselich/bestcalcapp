package com.givenocode.bestcalcapp.screen.reports

import android.support.annotation.IdRes
import com.givenocode.bestcalcapp.R

enum class FilterMode {
    DAY,
    WEEK,
    MONTH,
    YEAR,
    ALL
}

fun fromId(@IdRes id: Int): FilterMode =
    when (id) {
        R.id.group_day -> FilterMode.DAY
        R.id.group_week -> FilterMode.WEEK
        R.id.group_month -> FilterMode.MONTH
        R.id.group_year -> FilterMode.YEAR
        R.id.group_all -> FilterMode.ALL
        else -> throw IllegalStateException("Not supported id value")
    }


fun FilterMode.toId() =
        when (this) {
            FilterMode.DAY -> R.id.group_day
            FilterMode.WEEK -> R.id.group_week
            FilterMode.MONTH -> R.id.group_month
            FilterMode.YEAR -> R.id.group_year
            FilterMode.ALL -> R.id.group_all
        }