package com.givenocode.bestcalcapp.screen.reports

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.givenocode.bestcalcapp.BestCalcApp
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.databinding.FragmentReportsBinding
import com.givenocode.bestcalcapp.mvvm.ReportsViewModel
import kotlinx.android.synthetic.main.fragment_reports.*
import org.joda.time.LocalDate
import javax.inject.Inject

class ReportsFragment : Fragment() {

    companion object {
        private const val FIRST_DATE_KEY = "first"
        private const val SECOND_DATE_KEY = "second"

        fun newInstance(firstDate: LocalDate, secondDate: LocalDate): ReportsFragment {
            return ReportsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(FIRST_DATE_KEY, firstDate)
                    putSerializable(SECOND_DATE_KEY, secondDate)
                }
            }
        }
    }

    @Inject
    lateinit var viewModel: ReportsViewModel

    private var firstDate: LocalDate? = null
    private var secondDate: LocalDate? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        BestCalcApp.appComponent.inject(this)
        super.onCreate(savedInstanceState)

        arguments?.apply {
            firstDate = getSerializable(FIRST_DATE_KEY) as LocalDate
            secondDate = getSerializable(SECOND_DATE_KEY) as LocalDate
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentReportsBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_reports, container, false)
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // here we are sure that layout was already inflated
        activity?.setTitle(R.string.reports_title)

        rvReports.setHasFixedSize(true) // to improve performance

        viewModel.error.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(p0: Observable?, p1: Int) {
                var msg = viewModel.error.get()
                if (msg != null) {
                    msg = if (msg == "") getString(R.string.error) else msg
                    showError(msg, { viewModel.fetch() })
                }
            }
        })
        if (firstDate == null || secondDate == null) {
            viewModel.fetch()
        } else {
            viewModel.fetch(firstDate!!, secondDate!!)
        }
    }

    override fun onStop() {
        super.onStop()
        hideError()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDetach()
    }

    var snack: Snackbar? = null

    private fun showError(msg: String, action: () -> Unit) {
        if (view != null) {
            snack = Snackbar.make(view!!, msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry, { action.invoke() })
            snack?.show()
        }
    }

    private fun hideError() {
        snack?.dismiss()
    }
}
