package com.givenocode.bestcalcapp.screen.reports

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.givenocode.bestcalcapp.mvvm.ReportsTabsViewModel

class ReportsPagerAdapter(fm: FragmentManager, val viewModel: ReportsTabsViewModel) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        val dateRange = viewModel.getPageDateRange(position)
        return ReportsFragment.newInstance(dateRange.first, dateRange.second)
    }

    override fun getCount() = viewModel.getTabsCount()

    override fun getPageTitle(position: Int) = viewModel.getTabTitle(position)
}
