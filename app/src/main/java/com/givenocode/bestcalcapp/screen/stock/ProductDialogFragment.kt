package com.givenocode.bestcalcapp.screen.stock

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.view.Gravity
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import com.givenocode.bestcalcapp.BestCalcApp
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.commons.SquareImageView
import com.givenocode.bestcalcapp.commons.loadImg
import com.givenocode.bestcalcapp.commons.toDecimal
import com.givenocode.bestcalcapp.commons.toPrice
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.interactor.ProductsInteractor
import icepick.Icepick
import icepick.State
import org.jetbrains.anko.*
import org.jetbrains.anko.custom.ankoView
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File
import java.lang.Exception
import javax.inject.Inject

class ProductDialogFragment : DialogFragment() {

    companion object {
        private const val PRODUCT_ID_KEY = "PRODUCT_ID_KEY"
        private const val SHOW_DELETE_KEY = "SHOW_DELETE_KEY"

        private var onDelete: () -> Unit = {}
        private var onCreateUpdate: () -> Unit = {}

        fun newInstance(productId: Long? = null, showDelete: Boolean = false,
                        onDelete: () -> Unit = {},
                        onCreateUpdate: () -> Unit = {}
        ): ProductDialogFragment {
            this.onDelete = onDelete
            this.onCreateUpdate = onCreateUpdate

            return ProductDialogFragment().apply {
                arguments = Bundle().apply {
                    if (productId != null) {
                        putLong(PRODUCT_ID_KEY, productId)
                    }
                    putBoolean(SHOW_DELETE_KEY, showDelete)
                }
            }
        }
    }

    var etTitle: EditText? = null
    var etPrice: EditText? = null
    var ivPhoto: ImageView? = null

    @State
    @JvmField
    var imageFile: File? = null

    var productId: Long? = null
    var product: Product? = null
    var showDelete: Boolean? = null

    @Inject
    lateinit var productsInteractor: ProductsInteractor

    lateinit var ankoContext: AnkoContext<FrameLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        BestCalcApp.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        Icepick.restoreInstanceState(this, savedInstanceState)

        ankoContext = AnkoContext.create(context as Context, FrameLayout(context))

        arguments?.apply {
            productId = getLong(PRODUCT_ID_KEY)
            showDelete = getBoolean(SHOW_DELETE_KEY)
        }

        if (productId != null) {
            product = productsInteractor.get(productId!!)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val view = DialogUI().createView(ankoContext)
        val dialogBuilder = AlertDialog.Builder(context!!)
                .setView(view)
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null)

        if (showDelete == true) {
            dialogBuilder.setNeutralButton(R.string.delete, { _, _ -> showDeleteProductDialog() })
        }

        return dialogBuilder.create()
    }

    override fun onStart() {
        super.onStart()
        if (ivPhoto != null) {
            if (imageFile != null) {
                ivPhoto!!.loadImg(imageFile!!)
            } else if (product != null) {
                ivPhoto!!.loadImg(product!!.imageUrl)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val d = dialog as AlertDialog
        val positiveBtn = d.getButton(Dialog.BUTTON_POSITIVE)
        positiveBtn.setOnClickListener {
            if (validateFields()) {
                createOrUpdateProduct()
                d.dismiss()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Icepick.saveInstanceState(this, outState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {

            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                activity?.toast(R.string.default_error_message)
            }

            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                if (imageFile == null) {
                    activity?.toast(R.string.default_error_message)
                } else {
                    this@ProductDialogFragment.imageFile = imageFile
                    ivPhoto?.loadImg(imageFile)
                }
            }
        })
    }

    private fun invokePicker() {
        EasyImage.openDocuments(this, 0)
    }

    private fun createOrUpdateProduct() {
        if (product == null) {
            product = Product()
        }
        product!!.apply {
            name = etTitle?.text.toString()
            price = etPrice?.text.toString().toPrice()
            if (imageFile != null) {
                imageUrl = imageFile!!.path
            }
        }
        productsInteractor.insert(product!!)
        onCreateUpdate()
    }

    private fun validateFields(): Boolean {
        var isValid = false
        if (etTitle?.text?.isNotEmpty() == true
                && etPrice?.text?.isNotEmpty() == true) {
            isValid = true
        }

        if (!isValid) {
            ankoContext.toast(R.string.invalid_fields)
        }

        return isValid
    }

    private fun showDeleteProductDialog() {
        ankoContext.alert(getString(R.string.delete_confirm, product?.name ?: "")) {
            negativeButton(android.R.string.cancel) {}
            positiveButton(android.R.string.ok) {
                if (product != null) {
                    productsInteractor.delete(product!!)
                    onDelete()
                }
            }
        }.show()
        dismiss()
    }

    inner class DialogUI : AnkoComponent<ViewGroup> {

        private inline fun ViewManager.squareImageView(imageResource: Int, init: SquareImageView.() -> Unit) =
                ankoView({ SquareImageView(it) }, theme = 0) {
                    init()
                    setImageResource(imageResource)
                }

        override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
            scrollView {
                verticalLayout {

                    ivPhoto = squareImageView(R.drawable.product_placeholder) {
                        id = R.id.ivProductPhoto
                    }.lparams(width = matchParent, height = wrapContent) {
                        gravity = Gravity.CENTER_HORIZONTAL
                        margin = resources.getDimensionPixelOffset(R.dimen.padding_medium)
                    }
                    verticalLayout {

                        padding = resources.getDimensionPixelOffset(R.dimen.padding_medium)

                        button(R.string.btn_select_img_text)
                                .setOnClickListener { invokePicker() }
                        // stub view for focus request
                        editText {
                            isFocusable = true
                            isFocusableInTouchMode = true
                        }.lparams(width = dip(0), height = dip(0))
                        etTitle = editText {
                            id = R.id.etProductTitle
                            hintResource = R.string.title_hint
                            maxLines = 1
                            inputType = InputType.TYPE_CLASS_TEXT
                            setText(product?.name)
                        }.lparams(width = matchParent) {
                            topMargin = resources.getDimensionPixelOffset(R.dimen.margin_small)
                        }
                        etPrice = editText {
                            id = R.id.etProductPrice
                            hintResource = R.string.price_hint
                            inputType = InputType.TYPE_CLASS_NUMBER.or(InputType.TYPE_NUMBER_FLAG_DECIMAL)
                            setText(product?.price?.toDecimal() ?: "")
                        }.lparams(width = matchParent) {
                            topMargin = resources.getDimensionPixelOffset(R.dimen.margin_small)
                        }
                    }
                }
            }
        }
    }
}
