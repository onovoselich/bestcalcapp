package com.givenocode.bestcalcapp.screen.sale

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import com.givenocode.bestcalcapp.BestCalcApp
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.data.models.ReportItem
import com.givenocode.bestcalcapp.databinding.FragmentSaleBinding
import com.givenocode.bestcalcapp.interactor.ReportInteractor
import com.givenocode.bestcalcapp.mvvm.ProductsViewModel
import com.givenocode.bestcalcapp.screen.adapter.ProductsAdapter
import kotlinx.android.synthetic.main.fragment_sale.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.snackbar
import javax.inject.Inject

class SaleFragment : Fragment(), ProductsAdapter.OnProductClickListener {

    @Inject lateinit var viewModel: ProductsViewModel
    @Inject lateinit var reportInteractor: ReportInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        BestCalcApp.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentSaleBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_sale, container, false)
        binding.viewModel = viewModel
        binding.productListener = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // here we are sure that layout was already inflated
        activity?.setTitle(R.string.sale_title)

        rvProducts.setHasFixedSize(true) // to improve performance

        viewModel.error.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(p0: Observable?, p1: Int) {
                var msg = viewModel.error.get()
                if (msg != null) {
                    msg = if (msg == "") getString(R.string.error) else msg
                    showError(msg, { viewModel.fetch() })
                }
            }
        })
        viewModel.fetch()
    }

    override fun onStop() {
        super.onStop()
        hideError()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDetach()
    }

    override fun onProductClick(product: Product) {
        activity?.alert(product.name) {
            var counter: NumberPicker? = null
            customView {
                counter = numberPicker {
                    maxValue = 99
                    minValue = 1
                }
            }
            yesButton {
                val quantity = counter!!.value
                val reportItem = ReportItem(product.id, product.price, quantity)
                val insertedId: Long = reportInteractor.insert(reportItem)
                if (insertedId > 0) {
                    val message = resources.getQuantityString(R.plurals.product_sold_pattern, counter!!.value, counter!!.value, product.name)
                    snackbar(rvProducts, message, getString(R.string.revert)) {
                        reportInteractor.delete(reportItem.apply { id = insertedId })
                        snackbar(rvProducts, getString(R.string.reverted))
                    }
                }
            }
            cancelButton { }
        }?.show()
    }

    var snack: Snackbar? = null

    private fun showError(msg: String, action: () -> Unit) {
        if (view != null) {
            snack = Snackbar.make(view!!, msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry, { action.invoke() })
            snack?.show()
        }
    }

    private fun hideError() {
        snack?.dismiss()
    }
}
