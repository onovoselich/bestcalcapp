package com.givenocode.bestcalcapp.screen.stock

import android.databinding.DataBindingUtil
import android.databinding.Observable
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.givenocode.bestcalcapp.BestCalcApp
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.databinding.FragmentStockBinding
import com.givenocode.bestcalcapp.interactor.ProductsInteractor
import com.givenocode.bestcalcapp.mvvm.ProductsViewModel
import com.givenocode.bestcalcapp.screen.adapter.ProductsAdapter
import kotlinx.android.synthetic.main.fragment_stock.*
import org.jetbrains.anko.design.snackbar
import javax.inject.Inject

class StockFragment : Fragment(), ProductsAdapter.OnProductClickListener {

    companion object {
        private val DIALOG_TAG = "PRODUCT_DIALOG"
    }

    @Inject
    lateinit var viewModel: ProductsViewModel
    @Inject
    lateinit var productsInteractor: ProductsInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        BestCalcApp.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentStockBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_stock, container, false)
        binding.viewModel = viewModel
        binding.productListener = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // here we are sure that layout was already inflated
        activity?.setTitle(R.string.stock_title)

        rvProducts.setHasFixedSize(true) // to improve performance

        viewModel.error.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(p0: Observable?, p1: Int) {
                var msg = viewModel.error.get()
                if (msg != null) {
                    msg = if (msg == "") getString(R.string.error) else msg
                    showError(msg, { viewModel.fetch() })
                }
            }
        })
        viewModel.fetch()

        fab.setOnClickListener { openAddProductDialog() }
    }

    override fun onStop() {
        super.onStop()
        hideError()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDetach()
    }

    override fun onProductClick(product: Product) =
            openEditProductDialog(product)

    var snack: Snackbar? = null

    private fun showError(msg: String, action: () -> Unit) {
        if (view != null) {
            snack = Snackbar.make(view!!, msg, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry, { action.invoke() })
            snack?.show()
        }
    }

    private fun hideError() {
        snack?.dismiss()
    }

    private fun openEditProductDialog(product: Product) {
        ProductDialogFragment.newInstance(product.id, true,
                onDelete = {
                    snackbar(rvProducts, getString(R.string.delete), getString(R.string.revert)) {
                        productsInteractor.insert(product)
                        snackbar(rvProducts, getString(R.string.reverted))
                    }
                },
                onCreateUpdate = {
                    snackbar(rvProducts, getString(R.string.product_updated))
                })
                .show(childFragmentManager, DIALOG_TAG)
    }

    private fun openAddProductDialog() {
        ProductDialogFragment.newInstance(
                onCreateUpdate = {
                    snackbar(rvProducts, getString(R.string.product_created))
                }
        ).show(childFragmentManager, DIALOG_TAG)
    }
}
