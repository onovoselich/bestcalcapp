package com.givenocode.bestcalcapp.screen.reports

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.givenocode.bestcalcapp.BestCalcApp
import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.mvvm.ReportsTabsViewModel
import icepick.Icepick
import kotlinx.android.synthetic.main.fragment_reports_tabs.*
import javax.inject.Inject

class ReportsTabsFragment : Fragment() {

    @Inject
    lateinit var viewModel: ReportsTabsViewModel

    var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        BestCalcApp.appComponent.inject(this)
        super.onCreate(savedInstanceState)
        Icepick.restoreInstanceState(this, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_reports_tabs, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initPager()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // here we are sure that layout was already inflated
        activity?.setTitle(R.string.reports_title)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        this.menu = menu
        inflater?.inflate(R.menu.reports_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        this.menu = menu
        menu!!.findItem(viewModel.selectedFilter.toId()).isChecked = true
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        viewModel.selectedFilter = fromId(item!!.itemId)
        initPager()
        return super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Icepick.saveInstanceState(this, outState)
    }

    private fun initPager() {
        val adapter = ReportsPagerAdapter(fragmentManager!!, viewModel)
        pager.adapter = adapter
        pager.setCurrentItem(adapter.count - 1, false)
    }
}
