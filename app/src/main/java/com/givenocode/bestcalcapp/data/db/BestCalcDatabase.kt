package com.givenocode.bestcalcapp.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.givenocode.bestcalcapp.data.db.converters.DateConverter
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.data.models.ReportItem

@Database(entities = [(Product::class), (ReportItem::class)]
        , version = 1, exportSchema = false)
@TypeConverters(DateConverter::class)
abstract class BestCalcDatabase : RoomDatabase() {

    companion object {
        val NAME = "bestCalcApp_db"
    }

    abstract fun productsDao(): ProductsDao

    abstract fun reportsDao(): ReportsDao
}
