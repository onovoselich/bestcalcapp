package com.givenocode.bestcalcapp.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "products")
data class Product(
        var name: String = "",
        var price: Int = 0,
        var imageUrl: String = "",
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,
        var deleted: Boolean = false
)
