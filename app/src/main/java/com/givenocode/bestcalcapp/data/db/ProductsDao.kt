package com.givenocode.bestcalcapp.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.givenocode.bestcalcapp.data.models.Product
import io.reactivex.Flowable

@Dao
interface ProductsDao {

    @Query("SELECT * FROM products WHERE deleted=0")
    fun all(): Flowable<List<Product>>

    @Query("SELECT * FROM products WHERE id=:id")
    fun get(id: Long): Product

    @Insert(onConflict = REPLACE)
    fun insert(item: Product): Long

}
