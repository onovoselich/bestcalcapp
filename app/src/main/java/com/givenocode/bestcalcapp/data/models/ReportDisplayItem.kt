package com.givenocode.bestcalcapp.data.models

data class ReportDisplayItem(
        var name: String = "",
        var quantity: Int = 0,
        var totalPrice: Int = 0
)
