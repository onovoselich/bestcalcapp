package com.givenocode.bestcalcapp.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.givenocode.bestcalcapp.data.models.ReportItem
import io.reactivex.Flowable
import java.util.*

@Dao
interface ReportsDao {

    @Query("SELECT * FROM reports")
    fun all(): Flowable<List<ReportItem>>

    @Query("SELECT * FROM reports WHERE saleDate >= :start AND saleDate < :end")
    fun range(start: Date, end: Date): Flowable<List<ReportItem>>

    @Query("SELECT count(1) FROM reports WHERE saleDate >= :start AND saleDate < :end")
    fun countInRange(start: Date, end: Date): Int

    @Query("SELECT MIN(saleDate) FROM reports")
    fun oldestOrder(): Date?

    @Query("SELECT MAX(saleDate) FROM reports")
    fun newestOrder(): Date?

    @Insert
    fun insert(item: ReportItem): Long

    @Delete
    fun delete(item: ReportItem)
}
