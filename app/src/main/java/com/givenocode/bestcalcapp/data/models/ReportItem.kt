package com.givenocode.bestcalcapp.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import java.util.*


@Entity(tableName = "reports",
        foreignKeys = [(ForeignKey(entity = Product::class,
                parentColumns = arrayOf("id"),
                childColumns = arrayOf("productId")))])
data class ReportItem(
        var productId: Long = 0,
        var price: Int = 0,
        var quantity: Int = 0,
        var saleDate: Date = Date(System.currentTimeMillis()),
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0
)
