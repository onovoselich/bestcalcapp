package com.givenocode.bestcalcapp

import android.app.Application
import com.givenocode.bestcalcapp.di.AppComponent
import com.givenocode.bestcalcapp.di.AppModule
import com.givenocode.bestcalcapp.di.DaggerAppComponent
import net.danlew.android.joda.JodaTimeAndroid

class BestCalcApp : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        JodaTimeAndroid.init(this)

        appComponent = DaggerAppComponent
                .builder()
                .appModule(AppModule(this))
                .build()

    }
}
