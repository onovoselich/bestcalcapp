package com.givenocode.bestcalcapp.interactor

import android.util.Log
import com.givenocode.bestcalcapp.data.db.ProductsDao
import com.givenocode.bestcalcapp.data.db.ReportsDao
import com.givenocode.bestcalcapp.data.models.ReportDisplayItem
import com.givenocode.bestcalcapp.data.models.ReportItem
import com.givenocode.bestcalcapp.screen.reports.FilterMode
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.rxkotlin.toObservable
import io.reactivex.schedulers.Schedulers
import org.joda.time.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ReportInteractor
@Inject constructor(
        private val productsDao: ProductsDao,
        private val reportsDao: ReportsDao
) {

    fun insert(item: ReportItem) = reportsDao.insert(item)

    fun fetch(startDate: LocalDate, endDate: LocalDate): Flowable<List<ReportDisplayItem>> =
            reportsDao.range(startDate.toDate(), endDate.plusDays(1).toDate())
                    .flatMap {
                        it.toObservable()
                                .groupBy { it.productId }
                                .flatMap { mapToDisplayItem(it) }
                                .toList()
                                .toFlowable()
                    }
                    .observeOn(Schedulers.io())

    fun fetch(): Flowable<List<ReportDisplayItem>> =
            reportsDao.all()
                    .flatMap {
                        it.toObservable()
                                .groupBy { it.productId }
                                .flatMap { mapToDisplayItem(it) }
                                .toList()
                                .toFlowable()
                    }
                    .observeOn(Schedulers.io())


    private fun mapToDisplayItem(it: Observable<ReportItem>): Observable<ReportDisplayItem> {
        return it.collectInto(ReportDisplayItem(), { displayItem, reportItem ->
            with(displayItem) {
                if (name.isEmpty()) {
                    name = productsDao.get(reportItem.productId).name
                }
                quantity += reportItem.quantity
                totalPrice += reportItem.price * reportItem.quantity
            }

        }).toObservable()

    }

    fun delete(item: ReportItem) = reportsDao.delete(item)

    fun getTabs(selectedFilter: FilterMode): List<Pair<LocalDate, LocalDate>> {
        val oldestOrderDate = reportsDao.oldestOrder()

        return if (oldestOrderDate == null) {
            emptyList()
        } else {

            when (selectedFilter) {
                FilterMode.ALL ->
                    listOf(Pair(LocalDate.fromDateFields(oldestOrderDate),
                            LocalDate.fromDateFields(reportsDao.newestOrder())))
                else -> {
                    val count = getTabsCount(selectedFilter)
                    val list = mutableListOf<Pair<LocalDate, LocalDate>>()

                    for (i in 0.rangeTo(count)) {
                        val tabDatePair = getPageDateRange(i, count, selectedFilter)
                        if (tabDatePair != null) {
                            val tabCount = reportsDao.countInRange(tabDatePair.first.toDate(), tabDatePair.second.plusDays(1).toDate())
                            if (tabCount > 0) {
                                list.add(tabDatePair)
                            }
                        }
                    }
                    list
                }
            }
        }
    }

    fun getTabsCount(selectedFilter: FilterMode): Int {
        if (selectedFilter == FilterMode.ALL) {
            return 1
        }
        val oldestOrder = LocalDate.fromDateFields(reportsDao.oldestOrder() ?: return 1)
        val currentDate = LocalDate.now()

        return when (selectedFilter) {
            FilterMode.DAY -> Days.daysBetween(oldestOrder, currentDate).days + 1
            FilterMode.WEEK -> Weeks.weeksBetween(oldestOrder, currentDate).weeks + 1
            FilterMode.MONTH -> Months.monthsBetween(oldestOrder, currentDate).months + 1
            FilterMode.YEAR -> Years.yearsBetween(oldestOrder, currentDate).years + 1
            else -> 1
        }
    }

    fun getPageDateRange(position: Int, count: Int, selectedFilter: FilterMode): Pair<LocalDate, LocalDate>? {
        if (selectedFilter == FilterMode.ALL) {
            return null
        }
        val currentDate = LocalDate.now()

        return when (selectedFilter) {
            FilterMode.DAY -> {
                val date = currentDate.minusDays(count - position - 1)
                Pair(date, date)
            }
            FilterMode.WEEK -> {
                val date = currentDate.minusWeeks(count - position - 1)
                Pair(date.dayOfWeek().withMinimumValue(),
                        date.dayOfWeek().withMaximumValue())
            }
            FilterMode.MONTH -> {
                val date = currentDate.minusMonths(count - position - 1)
                Pair(date.dayOfMonth().withMinimumValue(),
                        date.dayOfMonth().withMaximumValue())
            }
            FilterMode.YEAR -> {
                val date = currentDate.minusYears(count - position - 1)
                Pair(date.dayOfYear().withMinimumValue(),
                        date.dayOfYear().withMaximumValue())
            }
            else -> null
        }
    }
}
