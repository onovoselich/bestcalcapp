package com.givenocode.bestcalcapp.interactor

import com.givenocode.bestcalcapp.data.db.ProductsDao
import com.givenocode.bestcalcapp.data.models.Product
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductsInteractor
@Inject constructor(private val productsDao: ProductsDao) {

    fun fetch(): Flowable<List<Product>> = productsDao.all()
            .observeOn(Schedulers.io())

    fun insert(product: Product) = productsDao.insert(product)

    fun update(product: Product) = productsDao.insert(product)

    fun delete(product: Product) = productsDao.insert(product.apply { deleted = true })

    fun get(id: Long) = productsDao.get(id)
}
