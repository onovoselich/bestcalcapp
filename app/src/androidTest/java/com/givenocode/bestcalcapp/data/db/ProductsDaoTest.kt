package com.givenocode.bestcalcapp.data.db

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.givenocode.bestcalcapp.data.models.Product
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ProductsDaoTest {

    private lateinit var productsDao: ProductsDao
    private var db: BestCalcDatabase? = null

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, BestCalcDatabase::class.java)
                .allowMainThreadQueries()
                .build()
        productsDao = db!!.productsDao()
    }

    @After
    fun closeDb() {
        db?.close()
    }

    @Test
    fun getWhenEmpty() {
        productsDao.all()
                .test()
                .assertValueCount(1)
                .assertValue({ it.isEmpty() })
    }

    @Test
    fun insertReturnsId() {
        val id = productsDao.insert(ITEM)

        assertEquals(ITEM.id, id)
    }

    @Test
    fun insertAndGetItem() {
        productsDao.insert(ITEM)

        productsDao.all().test()
                .assertValue({ it.size == 1 })
                .assertValue({ it[0].id == ITEM.id })
                .assertValue({ it[0].name == ITEM.name })
    }

    @Test
    fun getContainsDeleted() {
        productsDao.insert(Product(name = "baba"))
        productsDao.insert(Product(name = "lol", deleted = true))

        productsDao.all().test()
                .assertValue { it.size == 1 }
    }

    @Test
    fun getById() {
        val id: Long = 12234
        val item = ITEM.copy(id = id)
        productsDao.insert(item)

        val product = productsDao.get(id)
        assert(product.name == item.name)
    }

    @Test
    fun insertItemWithExistingNameShouldNotUpdateExisting() {
        productsDao.insert(ITEM)

        val anotherPrice = 123
        val anotherUrl = "anotherUrl"
        val anotherItem = Product(ITEM.name, anotherPrice, anotherUrl)

        productsDao.insert(anotherItem)

        productsDao.all().test()
                .assertValue({ it.size == 2 })
    }

    @Test
    fun updateItem() {
        // Given that we have an item in db
        productsDao.insert(ITEM)

        val newName = "matias"
        val updated = ITEM.copy(name = newName)
        productsDao.insert(updated)

        productsDao.all().test()
                .assertValue({ it.size == 1 })
                .assertValue({ it[0].name == newName })

    }

    companion object {
        val ITEM = Product("bob", 1000, "url", 123)
    }
}
