package com.givenocode.bestcalcapp.data.db

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.data.models.ReportItem
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class ReportsDaoTest {

    private lateinit var reportsDao: ReportsDao
    private var db: BestCalcDatabase? = null


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.inMemoryDatabaseBuilder(context, BestCalcDatabase::class.java)
                .allowMainThreadQueries()
                .build()
        db!!.productsDao().insert(PRODUCT_ITEM)
        reportsDao = db!!.reportsDao()
    }

    @After
    fun closeDb() {
        db?.close()
    }

    @Test
    fun getWhenEmpty() {
        reportsDao.all()
                .test()
                .assertValueCount(1)
                .assertValue({ it.isEmpty() })
    }

    @Test
    fun insertAndGetItem() {
        reportsDao.insert(ITEM)

        reportsDao.all().test()
                .assertValue { it.size == 1 }
                .assertValue { it[0].id == ITEM.id }
                .assertValue { it[0].price == ITEM.price }
    }

    @Test
    fun deleteItem() {
        reportsDao.insert(ITEM)

        reportsDao.delete(ITEM)

        reportsDao.all()
                .test()
                .assertValueCount(1)
                .assertValue { it.isEmpty() }
    }

    @Test
    fun dateFieldTest() {
        val date = Date(123456)
        val itemWithDate = ITEM.copy(saleDate = date)
        reportsDao.insert(itemWithDate)

        reportsDao.all().test()
                .assertValue { it[0].saleDate == date }
    }

    @Test
    fun range() {
        reportsDao.insert(ITEM.copy(id = 1, saleDate = Date(100)))
        reportsDao.insert(ITEM.copy(id = 2, saleDate = Date(10000)))
        reportsDao.insert(ITEM.copy(id = 3, saleDate = Date(1000000)))

        reportsDao.range(Date(999), Date(99999)).test()
                .assertValue { it.size == 1 }
                .assertValue { it[0].id == 2L }
    }

    @Test
    fun countInRangeTest() {
        reportsDao.insert(ITEM.copy(id = 1, saleDate = Date(100)))
        reportsDao.insert(ITEM.copy(id = 2, saleDate = Date(10000)))
        reportsDao.insert(ITEM.copy(id = 3, saleDate = Date(1000000)))

        val result = reportsDao.countInRange(Date(999), Date(99999))
        assertEquals(1, result)

        val result2 = reportsDao.countInRange(Date(10000), Date(1000001))
        assertEquals(2, result2)

        val result3 = reportsDao.countInRange(Date(0), Date(1000001))
        assertEquals(3, result3)
    }

    @Test
    fun oldestOrder() {
        reportsDao.insert(ITEM.copy(id = 1, saleDate = Date(100)))
        reportsDao.insert(ITEM.copy(id = 2, saleDate = Date(10000)))
        reportsDao.insert(ITEM.copy(id = 3, saleDate = Date(1000000)))

        assert(reportsDao.oldestOrder() == Date(100))
    }

    @Test
    fun newestOrder() {
        reportsDao.insert(ITEM.copy(id = 1, saleDate = Date(100)))
        reportsDao.insert(ITEM.copy(id = 2, saleDate = Date(10000)))
        reportsDao.insert(ITEM.copy(id = 3, saleDate = Date(1000000)))

        assert(reportsDao.oldestOrder() == Date(1000000))
    }

    @Test
    fun oldestOrderWhenEmpty() {
        assert(reportsDao.oldestOrder() == null)
    }

    companion object {
        val PRODUCT_ID: Long = 1
        val PRODUCT_ITEM = Product("name", 122, "url", PRODUCT_ID)
        val ITEM = ReportItem(PRODUCT_ID, 200, 5, id = 15)
    }
}
