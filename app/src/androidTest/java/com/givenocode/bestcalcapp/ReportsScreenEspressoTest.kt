package com.givenocode.bestcalcapp

import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import android.support.test.espresso.action.ViewActions.click
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.givenocode.bestcalcapp.screen.reports.ReportsTabsFragment
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ReportsScreenEspressoTest {

    @Rule
    @JvmField
    var rule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    val fragment = ReportsTabsFragment()

    private val ids = mapOf(
            R.id.group_day to R.string.group_day,
            R.id.group_week to R.string.group_week,
            R.id.group_month to R.string.group_month,
            R.id.group_year to R.string.group_year,
            R.id.group_all to R.string.group_all)

    @Before
    fun navigateToStock() {

        rule.activity.changeFragment(fragment)
    }

    @Test
    fun shouldApplyCorrectTitle() {
        toolbarWithTitle(R.string.reports_title) check isDisplayed
    }

    @Test
    fun shouldContainReportsList() {
        R.id.rvReports check isDisplayed
    }

    @Test
    fun shouldContainModeSwitch() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

        for (titleId in ids.values) {
            text(titleId) check isDisplayed
        }
    }

    @Test
    fun modeSwitchShouldBeSaved() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        for ((itemId, titleId) in ids) {
            text(titleId) perform click()

            openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
            assert(fragment.menu!!.findItem(itemId).isChecked)
            ids.keys.filter { it != itemId }
                    .map { fragment.menu!!.findItem(it) }
                    .forEach { assert(!it.isChecked) }
        }
    }
}
