package com.givenocode.bestcalcapp

import android.support.annotation.StringRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewAction
import android.support.test.espresso.ViewAssertion
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.v7.widget.Toolbar
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not

val isDisplayed: ViewAssertion = matches(ViewMatchers.isDisplayed())

val isNotDisplayed: ViewAssertion = matches(not(ViewMatchers.isDisplayed()))

fun toolbarWithTitle(@StringRes title: Int): ViewInteraction =
        onView(allOf(withText(title), withParent(isAssignableFrom(Toolbar::class.java))))

infix fun ViewInteraction.check(action: ViewAssertion): ViewInteraction = this.check(action)

infix fun ViewInteraction.perform(action: ViewAction): ViewInteraction = this.perform(action)

infix fun Int.check(action: ViewAssertion): ViewInteraction = onView(withId(this)).check(action)

infix fun Int.perform(action: ViewAction): ViewInteraction = onView(withId(this)).perform(action)

fun text(@StringRes resource: Int): ViewInteraction = onView(withText(resource))
