package com.givenocode.bestcalcapp

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.AppCompatCheckedTextView
import android.widget.ImageButton
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class BestCalcAppEspressoTest {

    @Rule
    @JvmField
    var rule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun navigateToStock() {
        onView(allOf(withParent(withId(R.id.toolbar)), isAssignableFrom(ImageButton::class.java)))  // Find menu button
                .perform(click())

        onView(withId(R.id.nav_view))
                .check(matches(isDisplayed()))

        onView(allOf(withText(R.string.menu_stock), isAssignableFrom(AppCompatCheckedTextView::class.java)))
                .perform(click())

        onView(withId(R.id.nav_view))
                .check(matches(not(isDisplayed())))

    }
}