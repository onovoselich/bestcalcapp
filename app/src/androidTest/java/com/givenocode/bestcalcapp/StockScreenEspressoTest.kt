package com.givenocode.bestcalcapp

import android.content.Context
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.inputmethod.InputMethodManager
import com.givenocode.bestcalcapp.screen.stock.StockFragment
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class StockScreenEspressoTest {

    @Rule
    @JvmField
    var rule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Before
    fun navigateToStock() {

        rule.activity.changeFragment(StockFragment())
    }

    @Test
    fun softKeyboardShownOnTitleFocus() {
        onView(withId(R.id.fab))
                .perform(click())

        onView(withId(R.id.etProductTitle))
                .perform(click())

        val imm = rule.activity
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        assertTrue(imm.isAcceptingText)
    }

    @Test
    fun softKeyboardShownOnPriceFocus() {
        onView(withId(R.id.fab))
                .perform(click())

        onView(withId(R.id.etProductPrice))
                .perform(click())

        val imm = rule.activity
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        assertTrue(imm.isAcceptingText)
    }
}