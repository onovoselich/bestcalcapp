package com.givenocode.bestcalcapp.di

import com.givenocode.bestcalcapp.R
import com.givenocode.bestcalcapp.data.db.BestCalcDatabase
import com.givenocode.bestcalcapp.data.models.Product
import com.givenocode.bestcalcapp.data.models.ReportItem
import kotlinx.android.synthetic.main.item_product.view.*
import java.util.*
import kotlin.collections.ArrayList

fun generateProducts(): List<Product> =
        ArrayList<Product>().apply {
            add(Product().apply {
                id = 1
                name = "Hot Dog"
                imageUrl = "file:///android_asset/hotdog.jpg"
                price = 300
            })
            add(Product().apply {
                id = 2
                name = "Pizza"
                imageUrl = "file:///android_asset/pizza.jpg"
                price = 150
            })
            add(Product().apply {
                id = 3
                name = "Chocolate pie"
                imageUrl = "file:///android_asset/pie.jpg"
                price = 300
            })
            add(Product().apply {
                id = 4
                name = "Coffee"
                imageUrl = "file:///android_asset/coffee.jpeg"
                price = 50
            })
            add(Product().apply {
                id = 5
                name = "Salad"
                imageUrl = "file:///android_asset/salad.jpeg"
                price = 500
            })
            add(Product().apply {
                id = 6
                name = "Cupcake"
                imageUrl = "file:///android_asset/cupcake.jpeg"
                price = 200
            })
        }

fun generateReports(product: Product): List<ReportItem> =
        ArrayList<ReportItem>().apply {
            add(ReportItem().apply {
                productId = product.id
                price = product.price
                quantity = 1
                saleDate = GregorianCalendar(2018, Calendar.JANUARY, 1).time
            })
            add(ReportItem().apply {
                productId = product.id
                price = product.price
                quantity = 2
                saleDate = GregorianCalendar(2018, Calendar.JANUARY, 15).time
            })
            add(ReportItem().apply {
                productId = product.id
                price = product.price
                quantity = 3
                saleDate = GregorianCalendar(2018, Calendar.JANUARY, 28).time
            })
            add(ReportItem().apply {
                productId = product.id
                price = product.price
                quantity = 4
                saleDate = GregorianCalendar(2018, Calendar.FEBRUARY, 1).time
            })
            add(ReportItem().apply {
                productId = product.id
                price = product.price
                quantity = 5
                saleDate = GregorianCalendar(2018, Calendar.FEBRUARY, 21).time
            })
        }


fun fillDatabase(database: BestCalcDatabase) {
    generateProducts().forEach {
        database.productsDao().insert(it)
        generateReports(it).forEach {
            database.reportsDao().insert(it)
        }
    }
}
