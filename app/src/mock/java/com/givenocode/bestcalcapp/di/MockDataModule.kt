package com.givenocode.bestcalcapp.di

import android.arch.persistence.room.Room
import android.content.Context
import com.givenocode.bestcalcapp.data.db.BestCalcDatabase
import com.givenocode.bestcalcapp.data.models.Product
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MockDataModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context) : BestCalcDatabase {
        val database = Room.inMemoryDatabaseBuilder(context, BestCalcDatabase::class.java)
                .allowMainThreadQueries()
                .build()

        fillDatabase(database)

        return database
    }

    @Provides
    @Singleton
    fun provideStockDao(db: BestCalcDatabase) = db.productsDao()

    @Provides
    @Singleton
    fun provideReportsDao(db: BestCalcDatabase) = db.reportsDao()

}
