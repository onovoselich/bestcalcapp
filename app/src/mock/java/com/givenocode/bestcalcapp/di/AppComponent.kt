package com.givenocode.bestcalcapp.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (MockDataModule::class)])
interface AppComponent : BaseAppComponent
