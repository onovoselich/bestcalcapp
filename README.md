

Best Calc App
=============
[![BuddyBuild](https://dashboard.buddybuild.com/api/statusImage?appID=59f75bde06651a000138b248&branch=master&build=latest)](https://dashboard.buddybuild.com/apps/59f75bde06651a000138b248/build/latest?branch=master)

This is a simple cash register app for android. It is designed for small retail places like a food trucks where there is no need for complex cash register software.


##### Available functionality:
1. Add/Edit/Delete product (`Stock` screen)
    * Each product has image, title and price
    * Image is selected from device gallery
    * When selected image renamed removed or deleted it detaches from product. After that operations product must be updated. THis behavior may be changed in future versions
2. Sale a product (`Sale` screen)
    * Quantity of product can be selected from picker. Allowed values: 1-99
    * After `Ok` clicked a record appears on `Reports` screen
3. See a list of reports(`Reports` screen)
    * You can see a separate reports for day, week, month and year groups
    * All records for a single product are grouped and total amount and price is displayed
    * Products with same name are not grouped
    * If product price changes report counts a price at the moment when a product was sold for each operation



##### Coming soon functionality:
1. Export reports to xml, pdf, etc.
2. Back Up data to cloud storage



Under the hood
==============

Project has `mock` and `pod` flavors. Mock flavor provides in-memory storage and some data inserted. This is useful both for manual and automated testing.

Development tools:
* Android Studio 3.0
* Gradle 4.1
* Kotlin 1.1
* BuddyBuild


Used libs and technologies:
* Kotlin with anko extensions
* MVVM + Android Data Binding
* TDD, Instrumented tests
* RxJava + Dagger2


Links to some of used materials:

 [Keddit - Awesome Kotlin tutorials series from Juan Ignacio Saravia](https://android.jlelse.eu/learn-kotlin-while-developing-an-android-app-introduction-567e21ff9664)

 [Advanced Androdi Espresso - Cool testing video tutorial by Chiu-KiChan](https://www.youtube.com/watch?v=JlHJFZvZyxw)

 [The MVC, MVP, and MVVM Smackdown by Eric Maxwell](https://academy.realm.io/posts/eric-maxwell-mvc-mvp-and-mvvm-on-android/)

 [Data binding in Android RecyclerView](http://blog.trsquarelab.com/2016/01/data-binding-in-android-recyclerview.html)